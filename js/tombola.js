$(document).ready(function(){

	// create an employee object which will hold all info
	// employee
	//		name 				: jack
	// 		noOfTokens 	: 10
	//		tokens 			: 2,7,18,29,30,32,38,39,47,61
	//		called			: (1-3) // once 3 we have a winner



	var employees = [];
	var tokenAssociation = [];
	var usedTokens = [];
	var clickReady = true;

	ui = $('#user-input');
	allo = $('#allocation');
	tom = $('#tombolla');
	win = $('#winner');
	
	allo.hide();
	tom.hide();
	win.hide();

	e = $('.employee');
	totalTokens = 0;

	var winner;
	// initiate object building using employees
	// when next button is pressed on data-input screen add No of tokens to object
	$('#continue').click(function(){
		if ($('#user-input').is(":visible") == true) {
			i = 0;
			e.each(function(){ 
				i++;
				t = this;
				n = $(t).find('h2').html();
				noTokens = $(t).find('input').val();
				totalTokens = totalTokens + parseFloat(noTokens);
				employees[i] = new employee(n, noTokens,"",0);
			});
			// make and array for totalTokens
			var tokens = [];
			for ( t = 0; t < totalTokens; t++ ) {
				tokens.push(t+1);
				tokenAssociation.push(t+1);
			}
			// shuffle array
			shuffle(tokens);
			j = 0;
			for ( emp in employees ){
				j++;
				// create a tokens array for allocated tokens
				employees[j].tokens = [];
				for(k = 0; k < employees[j].noOftokens; k++  ){
					// for each token that the employee has
					// pop number from tokens array and allocate to employee.tokens
					_t = tokens[0];
					tokens.shift();
					employees[j].tokens.push(_t);			
				}
			}
			$('#user-input').hide();
			allo.show();
		}

		// once allocation screen is made avaliable fire off the set interval to display 
		// tokens allocated
		if (allo.is(':visible')) {

			//for
			for ( employee in employees ) {
					html = "";
					employee_allocated = "<div class='tokens-allocated clearfix " + employees[employee].n +"'><h2>"+ employees[employee].n +"</h2><div class='tokens'>";
					html += employee_allocated;

					for (i = 0; i < employees[employee].tokens.length; i++ ) {
						tokens_allocated = "<span>" + employees[employee].tokens[i] + "</span>";
						html += tokens_allocated;
					}

					employee_allocated_end = "</div>";
					html += employee_allocated_end;

					allo.append(html);
			}	

			$('#continue').click(function(){ 
				allo.hide();
				tom.show();
				createTokenAssociation();


			});
		}
	});

	// tobolla time
	$('#roll').click(function(){

		if(clickReady){
			clickReady = false;
			$(this).addClass('disabled');
			ticker(300);

		}

		
	});
	/*
		running through set amount of frames and displaying a random number from the total number of tokens
		this is associated with am array to display the employee with that numbers mug shot when each tick is fired off

		once the set amount of frames have been fired it will compare the number to a usedToken array, if a match is found
		it will fire off the ticker function again until it finds a number which hasnt been found in usedToken and then fire
		off bank()
	*/
	function ticker(frames) {

		var timerun = 0;
		interval = setInterval(function(){
			if ( timerun != 0 ){
				$(".ticker #spin").removeClass();
			}		
			timerun++;				
			rand = Math.floor(Math.random()*totalTokens+1);
			$(".ticker #spin").html(rand);
			
			$(".ticker #spin").addClass(tokenAssociation[rand-1]);

			if ( timerun === frames ) {
				// check 
				if (usedTokens.length === 0 ){
					// put first one in
					usedTokens.push(rand);
					clearInterval(interval);
				$('#roll').removeClass('disabled');
				clickReady = true;	
				bank(rand);
				} else {
					
					for ( i = 0; i <= usedTokens.length; i++ ) {

						if ( rand === usedTokens[i] ){
							// roll again
							// console.error(rand);
							clearInterval(interval);
							ticker(2);
							return false;
						}
						
					}
					// put the number into used array
					clearInterval(interval);
					$('#roll').removeClass('disabled');
					clickReady = true;	
					bank(rand);
					usedTokens.push(rand);
				}
				
				

				// need to check a used numbers array here and auto increment again if it has been used
				//
				//
				
			}			
		},10);

		

	}

	// create a token list using the index of the array as token number 
	// and the vale as employee that has that token
	function createTokenAssociation() {
		for ( i = 0; i < tokenAssociation.length; i++ ){
			for(emp in employees) {
				for(j = 0; j < employees[emp].tokens.length; j++  ){
					if ( i+1 === employees[emp].tokens[j] ) {
						tokenAssociation[i] = employees[emp].n;
						break;
					}
				}
			}
		}
	}

	/*
		adds a class to the employee whos number has been found, to display an active blip under their portrait
		also adds increments the employees object.called 

		if the called == 3  it will fire off the winner screen after 2 seconds
	*/
	function bank(num) {

		for(emp in employees) {
			for(i = 0; i < employees[emp].tokens.length; i++  ){

				if (employees[emp].tokens[i] === num ) {
					employees[emp].called++ ;
					// add active class to a span
					name = employees[emp].n;
					counter = $('.employee_win_screen.'+name);
					span = counter.find('span');
					span.eq(employees[emp].called - 1).addClass('active');

					if ( employees[emp].called === 3 ){
						winner = employees[emp].n;
						// we have a winner
						setTimeout(function(){
							tom.hide();
							win.show();
						},2000);
						
						win.append("<div class='winner_mug_shot " +winner+ "'></div><h2 class='span12'>"+winner+ " has won the super mega awesome prize</h2>");
					}
				}

			}
		}
	}

	function employee (name, noTokens, tok, call){
		this.n = name;
		this.noOftokens = noTokens;
		this.tokens = tok;
		this.called = call;
	};

	function shuffle(o){ //v1.0
		for( var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
	};


});